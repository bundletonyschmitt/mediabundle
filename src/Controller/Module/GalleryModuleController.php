<?php

namespace TonySchmitt\MediaBundle\Controller\Module;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use TonySchmitt\MediaBundle\Entity\Module\GalleryModule;
use TonySchmitt\MediaBundle\Service\GalleryService;

class GalleryModuleController extends Controller
{

  private $galleryService;

  public function __construct(GalleryService $galleryService) {
    $this->galleryService = $galleryService;
  }

  public function galleryModuleAction($id, $args=array())
  {
    $em = $this->getDoctrine();

    $repository = $em->getRepository(GalleryModule::class);

    $galleryM = $repository->findOneBy(array("id" => $id));

    $idGallery = $galleryM->getGallery()->getId();

    $gallery = $this->galleryService->getGalleryMedias($idGallery);

    return $this->render('@TonySchmittMedia/galleryModule/gallery.html.twig',array('gallery' => $gallery));
  }
}
