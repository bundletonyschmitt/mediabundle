<?php

namespace TonySchmitt\MediaBundle\Controller\Module;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use TonySchmitt\MediaBundle\Entity\Module\ListGalleryModule;
use TonySchmitt\MediaBundle\Service\GalleryService;

class ListGalleryModuleController extends Controller
{

  private $galleryService;

  public function __construct(GalleryService $galleryService) {
    $this->galleryService = $galleryService;
  }

  public function listGalleryModuleAction($id, $args=array())
  {
    $em = $this->getDoctrine();

    $repository = $em->getRepository(ListGalleryModule::class);

    $galleryM = $repository->findOneBy(array("id" => $id));

    $gallery_display = false;

    if(isset($args) && count($args) > 0) {
      $galleries = $galleryM->getGalleries();
      foreach ($galleries as $value) {
        if($args[0] == $value->getSlug()) {
          $gallery_display = $value;
          break;
        }
      }
    }
    if(!$gallery_display) {
      return $this->render('@TonySchmittMedia/galleryModule/list-gallery.html.twig',array('galleries' => $galleryM->getGalleries()));
    } else {
      $gallery = $this->galleryService->getGalleryMedias($gallery_display->getId());

      return $this->render('@TonySchmittMedia/galleryModule/gallery.html.twig',array('gallery' => $gallery));
    }
    
  }
}
