<?php

namespace TonySchmitt\MediaBundle\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use TonySchmitt\MediaBundle\Service\MediaUploader;
use TonySchmitt\MediaBundle\Entity\Media;
use TonySchmitt\MediaBundle\Form\MediaType;

class MediaAdminController extends BaseAdminController
{
  private $mediaUploader;

  private $providerGet = 'provider';
  private $ytProvider = 'youtube';
  private $vimProvider = 'vimeo';
  private $imgProvider = 'image';
  private $fileProvider = 'file';
  private $bigFileProvider = 'bigFile';
  private $pdfProvider = 'pdf';

  public function __construct(MediaUploader $mediaUploader) {
    $this->mediaUploader = $mediaUploader;
  }

  protected function prePersistEntity($entity) {
    if(isset($_GET[$this->providerGet])) {
      $provider = $_GET[$this->providerGet];
    } else {
      $provider = $this->imgProvider;
    }
    switch ($provider) {
      case $this->ytProvider:
        $this->prePersistProviderYoutube($entity);
        $entity->setProvider($this->ytProvider);
        break;
      case $this->vimProvider:
        $this->prePersistProviderVimeo($entity);
        $entity->setProvider($this->vimProvider);
        break;
      case $this->fileProvider:
        $this->prePersistProviderFile($entity);
        $entity->setProvider($this->fileProvider);
        break;
      case $this->bigFileProvider:
        $this->prePersistProviderBigFile($entity);
        $entity->setProvider($this->bigFileProvider);
        break;
      case $this->pdfProvider:
        $this->prePersistProviderFile($entity);
        $entity->setProvider($this->pdfProvider);
        break;

      default:
        $this->prePersistProviderImage($entity);
        $entity->setProvider($this->imgProvider);
        break;
    }
  }

  protected function prePersistProviderImage($entity) {
    $uri = $this->mediaUploader->setImage($_FILES["media"]);
    $entity->setMediaFile(null);
    $entity->setMedia($uri["filename"]);
    $entity->setOriginalFilename($_FILES["media"]["name"]["mediaFile"]["file"]);
    $entity->setDirectory($uri["directory"]);
    $entity->setExtension($uri["extension"]);
    $entity->setName($_POST["media"]["name"]);
    $entity->setUpdatedAt(new \DateTime('now'));
  }

  protected function prePersistProviderFile($entity) {
    $uri = $this->mediaUploader->setFile($_FILES["media"]);
    $entity->setMediaFile(null);
    $entity->setMedia($uri["filename"]);
    $entity->setOriginalFilename($_FILES["media"]["name"]["mediaFile"]["file"]);
    $entity->setDirectory($uri["directory"]);
    $entity->setExtension($uri["extension"]);
    $entity->setName($_POST["media"]["name"]);
    $entity->setUpdatedAt(new \DateTime('now'));
  }

  protected function prePersistProviderYoutube($entity) {

      parse_str(parse_url($_POST["media"]["name"])["query"], $query);

      // Get content info of youtube
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/youtube/v3/videos?id=".$query["v"]."&part=snippet&key=AIzaSyBf8ifSzR_AgkkHKRVnj5Yjv9R09kfrZNk");
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     $output = curl_exec($ch);
     curl_close($ch);

     $snippet = json_decode($output)->items[0]->snippet;

     $entity->setName($snippet->title);
     $entity->setAuthor($snippet->channelTitle);
     $description = substr(str_replace("\r\n","",$snippet->description), 0, 510);
     $entity->setDescription($description);
     $entity->setMedia($query["v"]);
     $entity->setUpdatedAt(new \DateTime('now'));
     $entity->setDirectory($this->mediaUploader->setDirectory());
     $entity->setExtension(pathinfo($snippet->thumbnails->default->url, PATHINFO_EXTENSION));
  }

  protected function prePersistProviderVimeo($entity) {

      // Get content info of youtube
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, "https://vimeo.com/api/oembed.json?url=".$_POST["media"]["name"]);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     $output = curl_exec($ch);
     curl_close($ch);

     $snippet = json_decode($output);

     $entity->setName($snippet->title);
     $entity->setAuthor($snippet->author_name);
     $description = substr(str_replace("\r\n","",$snippet->description), 0, 510);
     $entity->setDescription($description);
     $entity->setMedia('/video/'.explode('/',$snippet->uri)[2]);
     $entity->setUpdatedAt(new \DateTime('now'));
     $entity->setDirectory($this->mediaUploader->setDirectory());
     $entity->setExtension(pathinfo($snippet->thumbnail_url, PATHINFO_EXTENSION));
  }

  protected function prePersistProviderBigFile($entity) {
    $uri = $this->mediaUploader->setBigFile($_POST["media"]["mediaFile"]["file"], $_POST["media"]["originalFilename"]);
    $entity->setOriginalFilename($_POST["media"]["originalFilename"]);
    $entity->setMediaFile(null);
    $entity->setMedia($uri["filename"]);
    $entity->setDirectory($uri["directory"]);
    $entity->setExtension($uri["extension"]);
    $entity->setName($_POST["media"]["name"]);
    $entity->setUpdatedAt(new \DateTime('now'));
  }

  protected function preUpdateEntity($entity) {
    if($mediaFile = $entity->getMediaFile()) {
      $uri = $this->mediaUploader->setMedia($mediaFile);
      $entity->setMediaFile(null);
      $entity->setMedia($uri["filename"]);
      $entity->setDirectory($uri["directory"]);
      $provider = $entity->getProvider();
      if($provider == $this->ytProvider || $provider == $this->vimProvider || $provider == $this->imgProvider) {
        $this->mediaUploader->setThumbMedia($entity);
      }
    }
  }

  protected function newAction() {
    if(!isset($_GET[$this->providerGet]) || ($_GET[$this->providerGet] != $this->imgProvider && $_GET[$this->providerGet] != $this->ytProvider && $_GET[$this->providerGet] != $this->vimProvider && $_GET[$this->providerGet] != $this->fileProvider && $_GET[$this->providerGet] != $this->pdfProvider && $_GET[$this->providerGet] != $this->bigFileProvider) ) {
      return $this->render('@TonySchmittMedia/media/select_provider_media.html.twig');
    }
    return parent::newAction();
  }

  protected function createNewForm($entity, array $entityProperties) {
    $media = new Media();
    return $this->get('form.factory')->create(MediaType::class, $media);
  }

}
