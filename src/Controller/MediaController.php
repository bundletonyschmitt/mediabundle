<?php

namespace TonySchmitt\MediaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use TonySchmitt\MediaBundle\Entity\Media;

class MediaController extends Controller
{
    /**
     * 
     */
    public function getMediaFileAction($id, $name) {
        $em = $this->getDoctrine()->getManager();
        
        $repository = $em->getRepository(Media::class);

        $media = $repository->findOneBy(array("id" => $id));

        $media->addHit();

        $em->persist($media);
        $em->flush();

        header('Content-disposition: attachment; filename="'.$name.'"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');

        header('Content-Length: '.filesize($_SERVER['DOCUMENT_ROOT']. $media->getDirectory() . $media->getMedia() ));
        header('Pragma: no-cache');
        header('Expires: 0');
        readfile($_SERVER['DOCUMENT_ROOT']. $media->getDirectory() . $media->getMedia() );
    }
}
