<?php

namespace TonySchmitt\MediaBundle\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use TonySchmitt\MediaBundle\Service\MediaUploader;
use TonySchmitt\MediaBundle\Entity\Gallery;
use TonySchmitt\MediaBundle\Entity\GalleryMedia;
use TonySchmitt\MediaBundle\Entity\Media;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Doctrine\Common\Collections\ArrayCollection;

class GalleryAdminController extends BaseAdminController
{

  protected function preUpdateEntity($entity) {
    $repositoryMedia = $this->getDoctrine()->getRepository(Media::class);
    $repositoryGallery = $this->getDoctrine()->getRepository(Gallery::class);
    $repositoryGalleryMedia = $this->getDoctrine()->getRepository(GalleryMedia::class);
    $i=0;

    $gallery_id = $entity->getId();

    $galleryMedia = $repositoryGalleryMedia->findBy(array("gallery" => $gallery_id));

    foreach ($galleryMedia as $value) {
      $this->getDoctrine()->getEntityManager()->remove($value);
      $this->getDoctrine()->getEntityManager()->flush($value);
    }

    $entity->setMedias(new ArrayCollection());

    if(isset($_POST["gallery"]["medias"])) {
      foreach ($_POST["gallery"]["medias"] as $value) {
        $galleryMedia = new GalleryMedia();
        $galleryMedia->setGallery($repositoryGallery->find($entity->getId()));
        $galleryMedia->setMedia($repositoryMedia->find($value));
        $galleryMedia->setWeight($i);
        $entity->addMedia($galleryMedia);
        $i++;
      }
    }

  }

}
