<?php

namespace TonySchmitt\MediaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use TonySchmitt\MediaBundle\Entity\Media;
use TonySchmitt\MediaBundle\Entity\Gallery;
use TonySchmitt\MediaBundle\Entity\GalleryMedia;
use TonySchmitt\MediaBundle\Service\MediaUploader;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminController extends Controller
{
    private $mediaUploader;

    public function __construct(MediaUploader $mediaUploader) {
        $this->mediaUploader = $mediaUploader;
    }

    /**
     * 
     */
    public function uploadMultipleFilesAction() {
        $em = $this->getDoctrine()->getManager();

        $directory_public = $this->container->getParameter('app.root.directory_public');
        $id_gallery = $_POST["id"];
        $repositoryMedia = $this->getDoctrine()->getRepository(Media::class);
        $repositoryGallery = $this->getDoctrine()->getRepository(Gallery::class);
        $repositoryGalleryMedia = $this->getDoctrine()->getRepository(GalleryMedia::class);
        $i=0;

        $gallery = $repositoryGallery->findOneBy(array("id" => $id_gallery));

        $galleryMedia = $repositoryGalleryMedia->findBy(array("gallery" => $id_gallery));

        $i = count($gallery->getMedias());

        $files = $_POST["files"];
        $medias = array();

        foreach ($files as $key => $value) {
            $image["name"]["mediaFile"]["file"] = $value[1];
            $image["tmp_name"]["mediaFile"]["file"] = $directory_public.'/uploads/bigfiles/'.$value[0];
            $return = $this->mediaUploader->setImageNoPost($image);
            $media = new Media();
            $media->setName($return['filename']);
            $media->setUpdatedAt(new \Datetime());
            $media->setExtension($return['extension']);
            $media->setDirectory($return['directory']);
            $media->setMedia($return['filename']);
            $media->setProvider('image');
            $media->setOriginalFilename($return['filename']);
            $em->persist($media);
            $em->flush();
            $this->mediaUploader->setThumbImage($media);

            $galleryMedia = new GalleryMedia();
            $galleryMedia->setGallery($gallery);
            $galleryMedia->setMedia($media);
            $galleryMedia->setWeight($i);
            $gallery->addMedia($galleryMedia);
            $em->persist($galleryMedia);
            $em->flush();
            $i++;
        }

        $em->persist($gallery);
        $em->flush();

        return new JsonResponse(array('status' => 'ok'));
    }
}
