<?php

namespace TonySchmitt\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="TonySchmitt\MediaBundle\Repository\GalleryRepository")
 */
class Gallery
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=255)
   */
  private $name;

  /**
   * @var string
   *
   * @ORM\Column(name="slug", type="string", length=255, nullable=true)
   */
  private $slug;

  /**
   * @ORM\Column(type="datetime")
   * @var \DateTime
   */
  private $updatedAt;

  /**
   * @ORM\OneToMany(targetEntity="TonySchmitt\MediaBundle\Entity\GalleryMedia", mappedBy="gallery", cascade={"persist", "remove"}, orphanRemoval=true)
   * @ORM\JoinColumn(nullable=true)
   * @ORM\OrderBy({"weight" = "asc"})
   */
  private $medias;

  public function __construct()
  {
    $this->updatedAt= new \Datetime();
    $this->medias = new ArrayCollection();
  }

  /**
   * Get the value of Id
   *
   * @return mixed
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * Set the value of Id
   *
   * @param mixed id
   *
   * @return self
   */
  public function setId($id)
  {
      $this->id = $id;

      return $this;
  }

  /**
   * Get the value of Name
   *
   * @return string
   */
  public function getName()
  {
      return $this->name;
  }

  /**
   * Set the value of Name
   *
   * @param string name
   *
   * @return self
   */
  public function setName($name)
  {
      $this->name = $name;

      return $this;
  }

  /**
   * Set the value of updatedAt
   *
   * @param \DateTime updatedAt
   *
   * @return self
   */
  public function setUpdatedAt($updatedAt)
  {
      $this->updatedAt = $updatedAt;

      return $this;
  }

  /**
   * Get the value of updatedAt
   *
   * @return \DateTime
   */
  public function getUpdatedAt()
  {
      return $this->updatedAt;
  }

  /**
   * Add media
   *
   * @param \TonySchmitt\MediaBundle\Entity\GalleryMedia $media
   *
   * @return Gallery
   */
  public function addMedia(GalleryMedia $media)
  {
    $this->medias->add($media);

    return $this;
  }

  public function removeMedia(GalleryMedia $media)
  {
    $this->medias->removeElement($media);
  }

  /**
   * @return Collection|GalleryMedia[]
   */
  public function getMedias()
  {
    return $this->medias;
  }

  public function setMedias($medias)
  {
    $this->medias = $medias;

    return $this;
  }

  public function __toString()
  {
    return $this->name;
  }

  /**
   * Get the value of slug
   *
   * @return  string
   */ 
  public function getSlug()
  {
    return $this->slug;
  }

  /**
   * Set the value of slug
   *
   * @param  string  $slug
   *
   * @return  self
   */ 
  public function setSlug(string $slug)
  {
    $this->slug = $slug;

    return $this;
  }
}
