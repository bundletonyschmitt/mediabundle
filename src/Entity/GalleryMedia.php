<?php

namespace TonySchmitt\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use TonySchmitt\MediaBundle\Entity\Gallery;
use TonySchmitt\MediaBundle\Entity\Media;

/**
 * @ORM\Entity(repositoryClass="TonySchmitt\MediaBundle\Repository\GalleryMediaRepository")
 */
class GalleryMedia
{
  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   *
   * @var integer $id
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="TonySchmitt\MediaBundle\Entity\Gallery", inversedBy="medias")
   */
  private $gallery;

  /**
   * @ORM\ManyToOne(targetEntity="TonySchmitt\MediaBundle\Entity\Media", cascade={"persist"})
   */
  private $media;

  /**
   * @var integer
   *
   * @ORM\Column(name="weight", type="integer", nullable=true)
   */
  private $weight;

  public function __construct()
  {
  }

  /**
   * Get the value of Id
   *
   * @return mixed
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * Set the value of Id
   *
   * @param mixed id
   *
   * @return self
   */
  public function setId($id)
  {
      $this->id = $id;

      return $this;
  }

  /**
   * Get gallery
   *
   * @return \TonySchmitt\MediaBundle\Entity\Gallery
   */
  public function getGallery()
  {
      return $this->gallery;
  }

  /**
   * Set gallery
   *
   * @param \TonySchmitt\MediaBundle\Entity\Gallery $gallery
   *
   * @return Gallery
   */
  public function setGallery(Gallery $gallery)
  {
      $this->gallery = $gallery;

      return $this;
  }

  /**
   * Get media
   *
   * @return \TonySchmitt\MediaBundle\Entity\Media
   */
  public function getMedia()
  {
      return $this->media;
  }

  /**
   * Set media
   *
   * @param \TonySchmitt\MediaBundle\Entity\Media $media
   *
   * @return GalleryMedia
   */
  public function setMedia(Media $media)
  {
      $this->media = $media;

      return $this;
  }

  /**
   * Get the value of Weight
   *
   * @return integer
   */
  public function getWeight()
  {
      return $this->weight;
  }

  /**
   * Set the value of Weight
   *
   * @param integer weight
   *
   * @return self
   */
  public function setWeight($weight)
  {
      $this->weight = $weight;

      return $this;
  }

  public function __toString()
  {
    return (string) $this->id;
  }

}
