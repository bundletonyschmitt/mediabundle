<?php

namespace TonySchmitt\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use TonySchmitt\MediaBundle\Service\MediaUploader;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="TonySchmitt\MediaBundle\Repository\MediaRepository")
 * @Vich\Uploadable
 */
class Media
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="directory", type="string", length=255, nullable=true)
     */
    private $directory;

    /**
     * @var string
     *
     * @ORM\Column(name="media", type="string", length=255)
     */
    private $media;

    /**
     * @Vich\UploadableField(mapping="directory_images", fileNameProperty="media")
     * @var File
     */
    private $mediaFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=511, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="copyright", type="string", length=255, nullable=true)
     */
    private $copyright;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=7, nullable=true)
     */
    private $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="provider", type="string", length=15, nullable=true)
     */
    private $provider;

    /**
     * @var string
     *
     * @ORM\Column(name="originalFilename", type="string", length=255, nullable=true)
     */
    private $originalFilename;

    /**
     * @var int
     *
     * @ORM\Column(name="hit", type="integer", nullable=true)
     */
    private $hit;

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Directory
     *
     * @return string
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * Set the value of Directory
     *
     * @param string directory
     *
     * @return self
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;

        return $this;
    }

    /**
     * Get the value of Media
     *
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set the value of Media
     *
     * @param string media
     *
     * @return self
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get the value of Media File
     *
     * @return string
     */
    public function getMediaFile()
    {
        return $this->mediaFile;
    }

    /**
     * Set the value of Media File
     *
     * @param string mediaFile
     *
     * @return self
     */
    public function setMediaFile($mediaFile)
    {
        $this->mediaFile = $mediaFile;

        if ($mediaFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Set the value of updatedAt
     *
     * @param \DateTime updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get the value of Author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set the value of Author
     *
     * @param string author
     *
     * @return self
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Copyright
     *
     * @return string
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * Set the value of Copyright
     *
     * @param string copyright
     *
     * @return self
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;

        return $this;
    }

    /**
     * Get the value of Extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set the value of Extension
     *
     * @param string extension
     *
     * @return self
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get the value of Provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set the value of Provider
     *
     * @param string provider
     *
     * @return self
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    public function __toString() {
        $string = $this->provider . ' - ' . $this->name;
        if($this->extension != '') {
            $string .= ' ( ' . $this->extension . ' )';
        }
        return $string;
    }


    /**
     * Get the value of originalFilename
     *
     * @return  string
     */ 
    public function getOriginalFilename()
    {
        return $this->originalFilename;
    }

    /**
     * Set the value of originalFilename
     *
     * @param  string  $originalFilename
     *
     * @return  self
     */ 
    public function setOriginalFilename(string $originalFilename)
    {
        $this->originalFilename = $originalFilename;

        return $this;
    }

    /**
     * Get the value of hit
     *
     * @return  int
     */ 
    public function getHit()
    {
        return $this->hit;
    }

    /**
     * Set the value of hit
     *
     * @param  int  $hit
     *
     * @return  self
     */ 
    public function setHit(int $hit)
    {
        $this->hit = $hit;

        return $this;
    }

    public function addHit() {
        if($this->hit == NULL) {
            $this->hit = 1;
        } else {
            $this->hit++;
        }
    }

    public function getThumbMedia($format) {
        $provider = $this->getProvider();
        if($provider === 'image' || $provider === 'youtube') {
            $directory = $this->getDirectory();
            $name = 'thumb_default_' . $this->getId() . '_' . $format . '.' . $this->getExtension();
            return $directory . $name;
        }
        return null;
    }

    public function getMediaUrl() {
        $directory = $this->getDirectory();
        $name = $this->getMedia();
        return $directory . $name;
    }
}
