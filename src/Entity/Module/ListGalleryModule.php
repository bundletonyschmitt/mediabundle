<?php

namespace TonySchmitt\MediaBundle\Entity\Module;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use TonySchmitt\MediaBundle\Entity\Gallery;

/**
 * @ORM\Entity(repositoryClass="TonySchmitt\MediaBundle\Repository\Module\ListGalleryModuleRepository")
 */
class ListGalleryModule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="TonySchmitt\MediaBundle\Entity\Gallery")
     */
    private $galleries;

    public function __construct() {
        $this->galleries = new ArrayCollection();
    }


    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    /**
     * Add gallery
     *
     * @param \TonySchmitt\MediaBundle\Entity\Gallery $gallery
     */
    public function addGallery(Gallery $gallery)
    {
    $this->galleries->add($gallery);

    return $this;
    }

    public function removeGallery(Gallery $gallery)
    {
    $this->galleries->removeElement($gallery);
    }


    /**
     * Get the value of galleries
     */ 
    public function getGalleries()
    {
        return $this->galleries;
    }

    /**
     * Set the value of galleries
     *
     * @return  self
     */ 
    public function setGalleries($galleries)
    {
        $this->galleries = $galleries;

        return $this;
    }
}
