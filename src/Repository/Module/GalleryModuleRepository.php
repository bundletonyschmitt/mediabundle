<?php

namespace TonySchmitt\MediaBundle\Repository\Module;

use TonySchmitt\MediaBundle\Entity\Module\GalleryModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class GalleryModuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GalleryModule::class);
    }
}
