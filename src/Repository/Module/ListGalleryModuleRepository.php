<?php

namespace TonySchmitt\MediaBundle\Repository\Module;

use TonySchmitt\MediaBundle\Entity\Module\ListGalleryModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ListGalleryModuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ListGalleryModule::class);
    }
}
