<?php

namespace TonySchmitt\MediaBundle\Repository;

use TonySchmitt\MediaBundle\Entity\GalleryMedia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class GalleryMediaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GalleryMedia::class);
    }
}
