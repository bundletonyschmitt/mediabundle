<?php

namespace TonySchmitt\MediaBundle\Service;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use TonySchmitt\MediaBundle\Entity\Gallery;
use TonySchmitt\MediaBundle\Repository\GalleryRepository;
use TonySchmitt\MediaBundle\Repository\MediaRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class GalleryService
{
    private $formats;
    private $repositoryGallery;
    private $repositoryMedia;

    private $thumb_name = 'thumb_default_';
    private $router;

    public function __construct($formats, GalleryRepository $repositoryGallery, MediaRepository $repositoryMedia, UrlGeneratorInterface $router) {
      $this->formats = $formats;
      $this->repositoryGallery = $repositoryGallery;
      $this->repositoryMedia = $repositoryMedia;
      $this->router = $router;
    }

    public function getGalleryMedias($galleryId) {

      $gallery = $this->repositoryGallery->findOneBy(array("id" => $galleryId));

      $return = array();
      $return['name'] = $gallery->getName();
      $return['updatedAt'] = $gallery->getUpdatedAt();
      $return['medias'] = array();

      foreach ($gallery->getMedias() as $galleryMedia) {
        $return['medias'][] = $this->getMediaInfo($galleryMedia->getMedia());

      }

      return $return;
    }

    public function getMedia($id) {
      $media = $this->repositoryMedia->findOneBy(array("id" => $id));

      return $this->getMediaInfo($media);
    }

    public function getMediaInfo($media) {
        $mediaValue = $media;

        $media = array();

        $media['name'] = $mediaValue->getName();
        $media['updatedAt'] = $mediaValue->getUpdatedAt();
        $media['author'] = $mediaValue->getAuthor();
        $media['description'] = $mediaValue->getDescription();
        $media['copyright'] = $mediaValue->getCopyright();
        $media['provider'] = $mediaValue->getProvider();

        $mediaPath = $mediaValue->getMedia();
        $mediaDirectory = $mediaValue->getDirectory();

        $originalFilename = $mediaValue->getOriginalFilename();
        if($originalFilename == '') {
          $originalFilename = $mediaValue->getMedia();
        }

        switch ($media['provider']) {
          case 'youtube':
            $mediaThumb = array();
            foreach ($this->formats as $name => $format) {
              $mediaThumb[$name] = $mediaDirectory.$this->thumb_name.$mediaValue->getId().'_'.$name.'.'.$mediaValue->getExtension();
            }
            $media['media'] = array(
              'id' => $mediaPath,
              'embed' => "https://www.youtube.com/embed/".$mediaPath,
              'link' => "https://youtu.be/".$mediaPath,
              'thumb' => $mediaThumb
            );
            break;
          case 'vimeo':
            $mediaThumb = array();
            foreach ($this->formats as $name => $format) {
              $mediaThumb[$name] = $mediaDirectory.$this->thumb_name.$mediaValue->getId().'_'.$name.'_'.$mediaValue->getExtension();
            }
            $media['media'] = array(
              'id' => $mediaPath,
              'embed' => "https://player.vimeo.com".$mediaPath,
              'link' => "https://vimeo.com".$mediaPath,
              'thumb' => $mediaThumb
            );
            break;
          case 'image':
            $mediaThumb = array();
            foreach ($this->formats as $name => $format) {
              $mediaThumb[$name] = $mediaDirectory.$this->thumb_name.$mediaValue->getId().'_'.$name.'.'.$mediaValue->getExtension();
            }
            $media['media'] = array(
              'link' => $mediaDirectory.$mediaPath,
              'thumb' => $mediaThumb
            );
            break;
          case 'bigFile':
          case 'file':
            $media['media'] = array(
              'link' => $this->router->generate('tonyschmitt_media_bigfile', array('id' => $mediaValue->getId(), 'name' => $originalFilename))
            );
            break;
          default:
            $media['media'] = array(
              'link' => $mediaDirectory.$mediaPath
            );
        }

        return $media;
    }
}
