<?php

namespace TonySchmitt\MediaBundle\Service;

use Symfony\Component\DependencyInjection\ContainerBuilder;

class MediaUploader
{
  private $public_directory;
  private $upload_directory_images;
  private $upload_directory_files;

  private $formats;

  private $thumb_name = 'thumb_default_';

  public function __construct($public_directory, $upload_directory_images, $upload_directory_files, $formats) {
    $this->public_directory = $public_directory;
    $this->upload_directory_images = $upload_directory_images;
    $this->upload_directory_files = $upload_directory_files;
    $this->formats = $formats;
  }

  public function setImage($image) {

    $dates = new \DateTime();

    // Crée le dossier pour les images
    if (!file_exists($this->public_directory.$this->upload_directory_images.'/'.$dates->format("y_m"))) {
      mkdir($this->public_directory.$this->upload_directory_images.'/'.$dates->format("y_m"), 0777, true);
    }

    // Generate a unique name for the file before saving it
    $fileName = $image["name"]["mediaFile"]["file"];

    if(file_exists($this->public_directory.$this->upload_directory_images.'/'.$dates->format("y_m").'/'.$fileName)) {
      $fileName = $dates->format("d_h_i_s_") . $fileName;
    }

    $directory = '/'.$dates->format("y_m").'/';

    $filename = $this->replaceSpecialCharacter($fileName);

    move_uploaded_file($image["tmp_name"]["mediaFile"]["file"], $this->public_directory.$this->upload_directory_images.$directory.$filename);

    $return["directory"] = $this->upload_directory_images.$directory;
    $return["filename"] = $filename;
    $return["extension"] = pathinfo($filename, PATHINFO_EXTENSION);

    return $return;

  }

  public function setImageNoPost($image) {

    $dates = new \DateTime();

    // Crée le dossier pour les images
    if (!file_exists($this->public_directory.$this->upload_directory_images.'/'.$dates->format("y_m"))) {
      mkdir($this->public_directory.$this->upload_directory_images.'/'.$dates->format("y_m"), 0777, true);
    }

    // Generate a unique name for the file before saving it
    $fileName = $image["name"]["mediaFile"]["file"];

    if(file_exists($this->public_directory.$this->upload_directory_images.'/'.$dates->format("y_m").'/'.$fileName)) {
      $fileName = $dates->format("d_h_i_s_") . $fileName;
    }

    $directory = '/'.$dates->format("y_m").'/';

    $filename = $this->replaceSpecialCharacter($fileName);

    rename($image["tmp_name"]["mediaFile"]["file"], $this->public_directory.$this->upload_directory_images.$directory.$filename);

    $return["directory"] = $this->upload_directory_images.$directory;
    $return["filename"] = $filename;
    $return["extension"] = pathinfo($filename, PATHINFO_EXTENSION);

    return $return;

  }

  public function setFile($file) {

    $dates = new \DateTime();

    // Crée le dossier pour les images
    if (!file_exists($this->public_directory.$this->upload_directory_files.'/'.$dates->format("y_m"))) {
      mkdir($this->public_directory.$this->upload_directory_files.'/'.$dates->format("y_m"), 0777, true);
    }

    // Generate a unique name for the file before saving it
    $fileName = $file["name"]["mediaFile"]["file"];

    if(file_exists($this->public_directory.$this->upload_directory_files.'/'.$dates->format("y_m").'/'.$fileName)) {
      $fileName = $dates->format("d_h_i_s_") . $fileName;
    }

    $directory = '/'.$dates->format("y_m").'/';

    $filename = $this->replaceSpecialCharacter($fileName);

    move_uploaded_file($file["tmp_name"]["mediaFile"]["file"], $this->public_directory.$this->upload_directory_files.$directory.$filename);

    $return["directory"] = $this->upload_directory_files.$directory;
    $return["filename"] = $filename;
    $return["extension"] = pathinfo($filename, PATHINFO_EXTENSION);

    return $return;

  }

  public function setBigFile($file, $fileName) {

    $dates = new \DateTime();

    // Crée le dossier pour les images
    if (!file_exists($this->public_directory.$this->upload_directory_files.'/'.$dates->format("y_m"))) {
      mkdir($this->public_directory.$this->upload_directory_files.'/'.$dates->format("y_m"), 0777, true);
    }

    if(file_exists($this->public_directory.$this->upload_directory_files.'/'.$dates->format("y_m").'/'.$fileName)) {
      $fileName = $dates->format("d_h_i_s_") . $fileName;
    }

    $directory = '/'.$dates->format("y_m").'/';

    $filename = $fileName;

    rename($this->public_directory.'/uploads/bigfiles/'.$file, $this->public_directory.$this->upload_directory_files.$directory.$filename);

    $return["directory"] = $this->upload_directory_files.$directory;
    $return["filename"] = $filename;
    $return["extension"] = pathinfo($filename, PATHINFO_EXTENSION);

    return $return;

  }


  public function setThumbImage($media) {
    foreach ($this->formats as $key => $value) {
      $this->resizeImage(
        $this->public_directory.$media->getDirectory().$media->getMedia(),
        $this->public_directory.$media->getDirectory().$this->thumb_name.$media->getId().'_'.$key.'.'.pathinfo($media->getMedia(), PATHINFO_EXTENSION),
        $value,
        $value
      );
    }
  }

  public function setThumbMediaYoutube($entity) {
    $idYoutube = $entity->getMedia();
    $id = $entity->getId();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/youtube/v3/videos?id=".$idYoutube."&part=snippet&key=AIzaSyBf8ifSzR_AgkkHKRVnj5Yjv9R09kfrZNk");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
    curl_close($ch);
    $snippet = json_decode($output)->items[0]->snippet;

    $img["small"] = $snippet->thumbnails->default->url;
    $img["medium"] = $snippet->thumbnails->medium->url;
    $img["big"] = $snippet->thumbnails->high->url;

    $directory = $entity->getDirectory();

    if (!file_exists($this->public_directory.$directory)) {
      mkdir($this->public_directory.$directory, 0777, true);
    }

    foreach ($img as $key => $value) {
      $imgFile = $this->public_directory.$directory.$this->thumb_name.$id.'_'.$key.'.'.pathinfo($value, PATHINFO_EXTENSION);
      file_put_contents($imgFile, fopen($value, 'r'));
    }
  }

  public function setThumbMediaVimeo($entity) {

     $id = $entity->getId();
     $idVimeo = $entity->getMedia();
      // Get content info of youtube
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, "https://vimeo.com/api/oembed.json?url=https://vimeo.com".$idVimeo);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     $output = curl_exec($ch);
     curl_close($ch);

     $snippet = json_decode($output);

     $img["small"] = $snippet->thumbnail_url;
     $img["medium"] = $snippet->thumbnail_url;
     $img["big"] = $snippet->thumbnail_url;

     $directory = $entity->getDirectory();

    if (!file_exists($this->public_directory.$directory)) {
      mkdir($this->public_directory.$directory, 0777, true);
    }

    foreach ($img as $key => $value) {
      $imgFile = $this->public_directory.$directory.$this->thumb_name.$id.'_'.$key.'.'.pathinfo($value, PATHINFO_EXTENSION);
      file_put_contents($imgFile, fopen($value, 'r'));
    }
  }

  public function setDirectory() {
    $dates = new \DateTime();
    return $this->upload_directory_images.'/'.$dates->format("y_m").'/';
  }

  public function replaceSpecialCharacter($name) {
    return str_replace(" ", "_", $name);
  }

  public function resizeImage($fileOriginal, $fileDestination, $maxWidth, $maxHeight) {

    // Calcul des nouvelles dimensions
    list($width, $height) = getimagesize($fileOriginal);
    if($width > $maxWidth && $width >= $height) {
      $percent = $maxWidth / $width;
    } else if($height > $maxHeight && $height > $width) {
      $percent = $maxHeight / $height;
    } else {
      copy($fileOriginal, $fileDestination);
      return $fileOriginal;
    }
    $newwidth = $width * $percent;
    $newheight = $height * $percent;

    // Chargement
    $thumb = imagecreatetruecolor($newwidth, $newheight);
    imagealphablending($thumb, false);
    imagesavealpha($thumb, true);
    $source = $this->imageExtension($fileOriginal);

    if($source == NULL) {
      return $fileOriginal;
    }

    // Redimensionnement
    imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    // Affichage
    return $this->imagesaveressource($thumb, $fileOriginal, $fileDestination);
  }

  public function imageExtension($img_src) {
    $extension = pathinfo($img_src, PATHINFO_EXTENSION);

    switch (strtolower($extension)) {
      case 'jpg':
      case 'jpeg':
        $img = imagecreatefromjpeg($img_src);
        break;
      case 'gif':
        $img = imagecreatefromgif($img_src);
        break;
      case 'png':
        $png = imagecreatefrompng($img_src);
        imagealphablending($png, true);
        $img = $png;
        break;
      default:
        $img = NULL;
        break;
    }

    return $img;
  }



  public function imagesaveressource ($thumb, $img_src, $destination) {
    $extension = pathinfo($img_src, PATHINFO_EXTENSION);

    if(file_exists($destination)) {
      unlink($destination);
    }

    switch (strtolower($extension)) {
      case 'jpg':
      case 'jpeg':
        imagejpeg($thumb, $destination);
        break;
      case 'gif':
        imagegif($thumb, $destination);
        break;
      case 'png':
        imagepng($thumb, $destination);
        break;
      default:
        $destination = false;
        break;
    }
    return $destination;
  }
}
