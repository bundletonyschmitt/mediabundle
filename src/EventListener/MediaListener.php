<?php

namespace TonySchmitt\MediaBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use TonySchmitt\MediaBundle\Entity\Media;
use TonySchmitt\MediaBundle\Service\MediaUploader;

class MediaListener
{
  private $mediaUploader;

  public function __construct(MediaUploader $mediaUploader) {
    $this->mediaUploader = $mediaUploader;
  }

  public function postPersist(LifecycleEventArgs $args)
  {
    $entity = $args->getEntity();

    if ($entity instanceof Media) {
      if(isset($_GET['provider'])) {
        $provider = $_GET['provider'];
      } else {
        $provider = 'image';
      }
      switch ($provider) {
        case 'youtube':
          $this->mediaUploader->setThumbMediaYoutube($args->getEntity());
          break;
        case 'vimeo':
          $this->mediaUploader->setThumbMediaVimeo($args->getEntity());
          break;

        case 'image':
          $this->mediaUploader->setThumbImage($args->getEntity());
          break;
      }
    }

  }
}
