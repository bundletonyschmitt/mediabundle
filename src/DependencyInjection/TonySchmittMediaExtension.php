<?php

namespace TonySchmitt\MediaBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

class TonySchmittMediaExtension extends extension implements PrependExtensionInterface
{
  public function load(array $configs, ContainerBuilder $container)
  {
    $loader = new YamlFileLoader(
      $container,
      new FileLocator(__DIR__.'/../Resources/config')
    );
  }

  public function prepend(ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
          $container,
          new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');

        $this->addAnnotatedClassesToCompile(array(
          'TonySchmitt\\MediaBundle\\Entity\\Media',
        ));
        $loader->load('easy_admin.yaml');
        $loader->load('twig.yaml');
        $loader->load('vich_uploader.yaml');
    }
}
