<?php

namespace TonySchmitt\MediaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Vich\UploaderBundle\Form\Type\VichImageType;
// ... use FormType... (text, choice...)

class MediaType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {

    if(isset($_GET['provider'])) {
      switch ($_GET['provider']) {
        case 'youtube':
          $builder
          ->add('name', TextType::class, array('label' => 'Lien Youtube :'))
          ;
          break;
        case 'vimeo':
          $builder
          ->add('name', TextType::class, array('label' => 'Lien Vimeo :'))
          ;
          break;
        case 'file':
          $builder
          ->add('name', TextType::class, array('label' => 'Nom du fichier :'))
          ->add('mediaFile', VichImageType::class, array('label' => 'Fichier :'))
          ;
          break;
        case 'bigFile':
          $builder
          ->add('name', TextType::class, array('label' => 'Nom du fichier :'))
          ->add('mediaFile', VichImageType::class, array('label' => 'Fichier :'))
          ->add('originalFilename', TextType::class, array('label' => 'Nom du fichier :'))
          ;
          break;
        case 'pdf':
          $builder
          ->add('name', TextType::class, array('label' => 'Nom du fichier PDF :'))
          ->add('mediaFile', VichImageType::class, array('label' => 'Fichier PDF :'))
          ;
          break;


        default:
          $builder
          ->add('name', TextType::class, array('label' => 'Nom de l\'image :'))
          ->add('mediaFile', VichImageType::class, array('label' => 'Image :'))
          ;
          break;
      }

    } else {
      $builder
      ->add('name', TextType::class, array('label' => 'Nom de l\'image :'))
      ->add('mediaFile', VichImageType::class, array('label' => 'Image :'))
      ;
    }
  }
}
