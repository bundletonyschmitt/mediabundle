<?php

namespace TonySchmitt\MediaBundle\Form\Module;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use TonySchmitt\MediaBundle\Entity\Module\ListGalleryModule;
// ... use FormType... (text, choice...)

class ListGalleryModuleType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
    ->add('id', HiddenType::class, array('required' => false))
    ->add('galleries')
    ;
  }

  /**
   * @param OptionsResolver $resolver
   */
  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults(array(
          'data_class' => 'TonySchmitt\MediaBundle\Entity\Module\ListGalleryModule'
      ));
  }
}
