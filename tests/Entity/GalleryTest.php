<?php

namespace TonySchmitt\MediaBundle\Tests\Entity;

use TonySchmitt\MediaBundle\Entity\Gallery;
use TonySchmitt\MediaBundle\Entity\GalleryMedia;
use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;

class GalleryTest extends TestCase
{
  /**
   * @var Gallery
   */
  protected $object;

  protected function setUp()
  {
    $this->object = new Gallery();
  }

  public function testGetterAndSetter()
  {
    $this->assertNull($this->object->getId());

    $id = 1;
    $this->object->setId($id);
    $this->assertEquals($id, $this->object->getId());

    $this->assertInstanceOf(\DateTime::class, $this->object->getUpdatedAt());
    $this->assertInstanceOf(ArrayCollection::class,$this->object->getMedias());

    $name = "galleryTest";
    $this->object->setName($name);
    $this->assertEquals($name, $this->object->getName());
    $this->assertEquals($name, $this->object->__toString());

    $updated = new \DateTime();
    $this->object->setUpdatedAt($updated);
    $this->assertEquals($updated, $this->object->getUpdatedAt());

    // Gallery Media
    $gallerym1 = new GalleryMedia();
    $gallerym1->setId(1);
    $gallerym2 = new GalleryMedia();
    $gallerym2->setId(2);
    $array = new ArrayCollection();
    $array->add($gallerym1);

    $this->object->addMedia($gallerym1);
    $this->assertEquals($array, $this->object->getMedias());

    $array->add($gallerym2);
    $this->object->setMedias($array);
    $this->object->removeMedia($gallerym1);
    $array->removeElement($gallerym1);
    $this->assertEquals($array, $this->object->getMedias());
  }
}
