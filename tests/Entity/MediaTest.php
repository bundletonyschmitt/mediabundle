<?php

namespace TonySchmitt\MediaBundle\Tests\Entity;

use TonySchmitt\MediaBundle\Entity\Media;
use PHPUnit\Framework\TestCase;

class MediaTest extends TestCase
{
  /**
   * @var Media
   */
  protected $object;

  protected function setUp()
  {
    $this->object = new Media();
  }

  public function testGetterAndSetter()
  {
    $this->assertNull($this->object->getId());

    $id = 1;
    $this->object->setId($id);
    $this->assertEquals($id, $this->object->getId());

    $name = "imageTest";
    $this->object->setName($name);
    $this->assertEquals($name, $this->object->getName());
    $this->assertEquals($name, $this->object->__toString());

    $directory = "/uploads/images";
    $this->object->setDirectory($directory);
    $this->assertEquals($directory, $this->object->getDirectory());

    $media = "media.png";
    $this->object->setMedia($media);
    $this->assertEquals($media, $this->object->getMedia());

    $mediaFile = "mediaFile.jpg";
    $this->object->setMediaFile($mediaFile);
    $this->assertEquals($mediaFile, $this->object->getMediaFile());
    $this->assertInstanceOf(\DateTime::class, $this->object->getUpdatedAt());

    $updated = new \DateTime('now');
    $this->object->setUpdatedAt($updated);
    $this->assertEquals($updated, $this->object->getUpdatedAt());

    $author = "Auteur";
    $this->object->setAuthor($author);
    $this->assertEquals($author, $this->object->getAuthor());

    $description = "Je suis une description.";
    $this->object->setDescription($description);
    $this->assertEquals($description, $this->object->getDescription());

    $copyright = "Je suis un copyright.";
    $this->object->setCopyright($copyright);
    $this->assertEquals($copyright, $this->object->getCopyright());

    $extension = "gif";
    $this->object->setExtension($extension);
    $this->assertEquals($extension, $this->object->getExtension());

    $provider = "image";
    $this->object->setProvider($provider);
    $this->assertEquals($provider, $this->object->getProvider());

  }
}
