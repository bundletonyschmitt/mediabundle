<?php

namespace TonySchmitt\MediaBundle\Tests\Entity;

use TonySchmitt\MediaBundle\Entity\GalleryMedia;
use TonySchmitt\MediaBundle\Entity\Gallery;
use TonySchmitt\MediaBundle\Entity\Media;
use PHPUnit\Framework\TestCase;

class GalleryMediaTest extends TestCase
{
  /**
   * @var GalleryMedia
   */
  protected $object;

  protected function setUp()
  {
    $this->object = new GalleryMedia();
  }

  public function testGetterAndSetter()
  {
    $this->assertNull($this->object->getId());

    $id = 1;
    $this->object->setId($id);
    $this->assertEquals($id, $this->object->getId());
    $this->assertEquals((string) $id, $this->object->__toString());

    $gallery = new Gallery();
    $this->object->setGallery($gallery);
    $this->assertEquals($gallery, $this->object->getGallery());

    $media = new Media();
    $this->object->setMedia($media);
    $this->assertEquals($media, $this->object->getMedia());

    $weight = 2;
    $this->object->setWeight($weight);
    $this->assertEquals($weight, $this->object->getWeight());

  }
}
