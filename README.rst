Après installation

Configure le fichier

config/packages/oneup_uploader.yaml
"
# Read the documentation: https://github.com/1up-lab/OneupUploaderBundle/blob/master/Resources/doc/index.md
oneup_uploader:
    mappings:
        # This is a mapping example, remove it and create your own mappings.
        gallery:
            frontend: fineuploader # or any uploader you use in the frontend
            storage:
                directory: '%kernel.project_dir%/public/uploads/bigfiles'
    chunks:
        maxage: 86400
        storage:
            directory: '%kernel.cache_dir%/uploads/chunks'
"